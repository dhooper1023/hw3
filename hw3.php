<html>
<body>
<?php
  echo "Ethan Hooper<br>";
  
  #Opening the file
  $myfile = "hw3.txt";
  @ $fp = fopen("$myfile", "r");
  if (!$fp)
  {
	  echo "<p>Could not read $myfile";
	  exit;
  }
  
  #Read the 5 lines
  $readline1 = substr(fgets($fp, 60), 3);
  $readline2 = substr(fgets($fp, 60), 3);
  $readline3 = substr(fgets($fp, 60), 3);
  $readline4 = substr(fgets($fp, 60), 3);
  $readline5 = substr(fgets($fp, 60), 3);
  echo "Five lines read.<br>";
  echo "$readline1 <br>";
  echo "$readline2 <br>";
  echo "$readline3 <br>";
  echo "$readline4 <br>";
  echo "$readline5 <br>";
  
  #Check if we really got to the end
  if (feof($fp)) 
  {
    echo "End of Line.<br>";
  }
  else 
  {
    echo "No, there is more where that came from.<br>"; 
  }
  
  #Close out the file
  fclose($fp);
  exit;
  
?>
</body>
</html>